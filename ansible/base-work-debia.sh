#!/bin/bash

#sudo apt-get -y install git vim mc wget build-essential
#wget https://www.python.org/ftp/python/3.9.5/Python-3.9.5.tgz -O /tmp/python.tgz
#tar -xzf /tmp/python.tgz -C /tmp/
cd /tmp/Python-3.9.5
./configure --enable-optimizations --with-ensurepip=install --with-ssl-default-suites=python
make
sudo make install

sudo update-alternatives --install /usr/bin/python python /usr/local/bin/python3 1
sudo update-alternatives --install /usr/bin/pip pip /usr/local/bin/pip3 1

pip install ansible
