#!/bin/sh

# PROVIDE: wsdd
# REQUIRE: DAEMON samba_server
# BEFORE: LOGIN
# KEYWORD: shutdown

# Add the following line in /etc/rc.conf to enable wsdd:
#
# wsdd_enable="YES"
# wsdd_flags="<set as needed>"
# wsdd_domain="<set if your host is an AD member>"
# wsdd_group="<set if you want to set the workgroup manually>"
#
# Do not specify -d DOMAIN or -w WORKGROUP in wsdd_flags. Instead, set
# wsdd_domain="DOMAIN" or wsdd_group="WORKGROUP", otherwise it will be
# overridden by automatically detected workgroup.
#

. /etc/rc.subr

name=wsdd
rcvar=wsdd_enable

load_rc_config ${name}

: ${wsdd_enable:="NO"}
: ${wsdd_flags:=""}
: ${wsdd_domain:=""}
: ${wsdd_group:=""}
: ${wsdd_smb_config_file:="/usr/local/etc/smb4.conf"}

if [ -z "${wsdd_group}" ]; then
        # automatic detection of workgroup
        wsdd_group=$(/usr/local/bin/testparm -s --parameter-name workgroup 2>/dev/null)

        # try to manually extract workgroup from samba configuration if testparm failed
        if [ -z "$wsdd_group" ] && [ -r $wsdd_smb_config_file ]; then
                wsdd_group="$(grep -i '^[[:space:]]*workgroup[[:space:]]*=' $wsdd_smb_config_file | cut -f2 -d= | tr -d '[:blank:]')"
        fi
fi

wsdd_opts="-n storage02.int.zxyxy.net -p -H 4"
if [ -n "${wsdd_flags}" ]; then
        wsdd_opts="${wsdd_flags}"
fi
if [ -n "$wsdd_domain" ]; then
        wsdd_opts="${wsdd_opts} -d ${wsdd_domain}"
elif [ -n "$wsdd_group" ]; then
        wsdd_opts="${wsdd_opts} -w ${wsdd_group}"
fi

command="/usr/local/bin/wsdd"
procname="/usr/local/bin/python3.8"
pidfile="/var/run/${name}.pid"

start_cmd="${name}_start"

wsdd_start()
{
        echo -n "Starting ${name}."
        /usr/sbin/daemon -u _wsdd -S -p ${pidfile} ${command} ${wsdd_opts}
}

run_rc_command "$1"
