if [ -z $1 ]; then 
    ansible-playbook -i inventory.yaml playbook.yaml  --ask-vault-password
else 
    ansible-playbook -i inventory.yaml playbook.yaml  --ask-vault-password -l $1
fi
