(set-frame-font "hack")
(set-cursor-color "green")
;(set-mouse-color "goldenrod")
;(set-face-background 'region "black")
(set-background-color "black")
(set-foreground-color "green")
(set-face-attribute 'default (selected-frame) :height 125)

(setq inhibit-startup-screen t)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(global-auto-revert-mode t)

(set-face-foreground 'mode-line "#F0F0F0")
(set-face-background 'mode-line "#4040FF")
(set-face-foreground 'mode-line-inactive "#606060")
(set-face-background 'mode-line-inactive "#202020")

(setq tab-width 4
      indent-tabs-mode nil)

(when window-system (global-hl-line-mode -1))
