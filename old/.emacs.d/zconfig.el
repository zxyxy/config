(use-package which-key
  :ensure t
  :init
  (which-key-mode))


;(use-package dakrone-theme
;    :ensure t)

(use-package company
  :ensure t)

(use-package company-irony
  :ensure t)

(use-package cmake-mode
  :ensure t)

;(use-package cmake-ide
;  :ensure t)

(use-package cmake-project
  :ensure t)

(use-package python
  :ensure t)

(use-package python-mode
  :ensure t)

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

;(use-package flycheck-irony
;  :ensure t)

;(use-package flycheck-rtags
;  :ensure t)

(use-package git
  :ensure t)

(use-package gitconfig
  :ensure t)

(use-package gitconfig-mode
  :ensure t)

(use-package org
  :ensure t
  :pin manual)

(use-package gitlab
  :ensure t)

(use-package markdown-mode
  :ensure t)

(use-package meson-mode
  :ensure t)

(use-package beacon
  :ensure t
  :init
  (beacon-mode t))


;(setq initial-major-mode 'text-mode)

;(add-hook 'after-init-hook 'global-company-mode)
;(add-hook 'after-init-hook 'global-flycheck-mode)

(load-file "~/.emacs.d/robot-mode.el")
(add-to-list 'auto-mode-alist '("\\.robot\\'" . robot-mode))

(setq make-backup-files nil)

(defalias 'yes-or-no-p 'y-or-n-p)

(setq column-number-mode t)


(add-hook 'c++-mode-hook
          (lambda () (setq flycheck-clang-include-path
                           (list (expand-file-name "~/local/include/")))))

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

(eval-after-load 'company
  '(setq company-idle-delay 0))

(eval-after-load 'company
  '(setq company-minimum-prefix-length 2))


(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c++-mode-hook 'company-mode)
;(add-hook 'c++-mode-hook 'flycheck-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'company-mode)
;(add-hook 'c-mode-hook 'flycheck-mode)
(add-hook 'objc-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'company-mode)
;(add-hook 'objc-mode-hook 'flycheck-mode)
;(add-hook 'python-mode-hook 'company-mode)
;(add-hook 'python-mode-hook 'flycheck-mode)
(add-hook 'org-mode-hook 'company-mode)

;(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(add-hook 'python-mode-hook 'jedi:setup)
(add-hook 'python-mode-hook 'jedi:ac-setup)

(add-to-list 'auto-mode-alist '("\\.ledger\\'" . ledger-mode))

(setq python-shell-interpreter "ipython"
    python-shell-interpreter-args "--simple-prompt -i")

(setq make-backup-file nil)
(setq auto-save-default nil)

(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode t)

(use-package ido-vertical-mode
   :ensure t
   :init
   (ido-vertical-mode 1))

(setq ido-vertical-define-keys 'C-n-and-C-p-only)

(setq org-support-shift-select t)

(load-library "find-lisp")
(setq org-agenda-files1 (find-lisp-find-files "~/Documents/org" "\.org$"))
(setq org-agenda-files (append (find-lisp-find-files "~/Dropbox/org" "\.org$") org-agenda-files1 ))

(setq org-agenda-span 30
      org-agenda-start-day "-3d")

(with-eval-after-load 'org-agenda
  (setq org-agenda-prefix-format '((todo . "  %b"))))

(use-package org-bullets
   :ensure t
   :config
   (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))

(setq org-src-window-setup 'current-window)

(use-package ledger-mode
  :ensure t)

(add-hook 'ledger-mode-hook 'company-mode)

(use-package logview
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.log\\'" . logview-mode))

(use-package ansible
  :ensure t)

(use-package company-ansible
  :ensure t)

(set-frame-font "hack")
(set-cursor-color "green")
;(set-mouse-color "goldenrod")
;(set-face-background 'region "black")
(set-background-color "black")
;(Set-foreground-color "green")
;(set-face-attribute 'default (selected-frame) :height 100)

(setq inhibit-startup-screen t)

(add-to-list 'load-path "~/.emacs.d/vendor/emacs-powerline")

(require 'powerline)

;git clone git://github.com/jonathanchu/emacs-powerline.git
(custom-set-faces
 '(mode-line-inactive ((t (:foreground "#f9f9f9" :background "#666666" :box nil)))))

(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(global-auto-revert-mode t)

(set-face-foreground 'mode-line "#F0F0F0")
(set-face-background 'mode-line "#4040FF")
(set-face-foreground 'mode-line-inactive "#606060")
(set-face-background 'mode-line-inactive "#202020")

(setq tab-width 4
      indent-tabs-mode nil)

(when window-system (global-hl-line-mode -1))

(use-package pretty-mode
  :ensure t)

(require 'pretty-mode)
(global-pretty-mode t)
(pretty-activate-groups
 '(:sub-and-superscripts :greek :arithmetic-nary :equality :ordering :ordering-double :ordering-triple
             :arrows :arrows-twoheaded :punctuation
             :logic :sets ))

(global-prettify-symbols-mode t)

(add-hook
 'python-mode-hook
 (lambda ()
   (mapc (lambda (pair) (push pair prettify-symbols-alist))
         '(;; Syntax
           ("def" .      #x0192)
           ("not" .      #x2757)
           ("in" .       #x2208)
           ("not in" .   #x2209)
           ("return" .   #x27fc)
           ("yield" .    #x27fb)
           ("for" .      #x2200)
           ;; Base Types
           ("int" .      #x2124)
           ("float" .    #x211d)
           ("str" .      #x1d54a)
           ("True" .     #x1d54b)
           ("False" .    #x1d53d)
           ;; Mypy
           ("Dict" .     #x1d507)
           ("List" .     #x2112)
           ("Tuple" .    #x2a02)
           ("Set" .      #x2126)
           ("Iterable" . #x1d50a)
           ("Any" .      #x2754)
           ("Union" .    #x22c3)))))

(global-set-key (kbd "C-x b") 'ibuffer)
(global-set-key (kbd "C-x C-b") 'ido-switch-buffer)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq-default show-trailing-whitespace t)

(defvar zterm "/bin/bash")
(defadvice ansi-term (before force-bash)
  (interactive (list zterm)))
(ad-activate 'ansi-term)

(global-set-key (kbd "C-t") 'ansi-term)
