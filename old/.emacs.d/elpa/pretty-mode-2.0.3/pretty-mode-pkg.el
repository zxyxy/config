;;; -*- no-byte-compile: t -*-
(define-package "pretty-mode" "2.0.3" "Redisplay parts of the buffer as pretty symbols." 'nil :commit "4ba8fceb7dd733361ed975d80ac2caa3612fa78b" :keywords '("pretty" "unicode" "symbols") :url "https://github.com/akatov/pretty-mode")
