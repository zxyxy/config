;;; -*- no-byte-compile: t -*-
(define-package "auto-compile" "1.4.3" "automatically compile Emacs Lisp libraries" '((emacs "24.3") (packed "2.0.0")) :commit "6ce4255ab9a0b010ef8414c5bd9a6d6d9eea012f" :keywords '("compile" "convenience" "lisp") :url "https://github.com/emacscollective/auto-compile")
