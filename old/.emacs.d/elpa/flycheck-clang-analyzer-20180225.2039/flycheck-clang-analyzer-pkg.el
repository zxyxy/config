;;; -*- no-byte-compile: t -*-
(define-package "flycheck-clang-analyzer" "20180225.2039" "Integrate Clang Analyzer with flycheck" '((flycheck "0.24") (emacs "24.4")))
