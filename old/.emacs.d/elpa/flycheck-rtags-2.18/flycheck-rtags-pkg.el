;;; -*- no-byte-compile: t -*-
(define-package "flycheck-rtags" "2.18" "RTags Flycheck integration." '((emacs "24") (flycheck "0.23") (rtags "2.10")))
