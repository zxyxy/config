;;; code:


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'package)

(add-to-list 'package-archives
;	     '("melpa" . "https://melpa.org/packages"))
;            '("melpa" . "http://melpa.milkbox.net/packages/"))
             '("melpa" . "http://stable.melpa.org/packages/"))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(org-babel-load-file (expand-file-name "~/.emacs.d/zconfig.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (which-key use-package sr-speedbar robots-txt-mode python-mode projectile pretty-mode org-bullets org meson-mode markdown-mode ledger-mode jedi ido-vertical-mode gitlab gitconfig-mode gitconfig git flycheck-rtags flycheck-pycheckers flycheck-ledger flycheck-irony flycheck-clang-analyzer ecb dakrone-theme company-jedi company-irony cmake-project cmake-mode beacon auto-compile async))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mode-line-inactive ((t (:foreground "#f9f9f9" :background "#666666" :box nil)))))
